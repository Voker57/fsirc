#ifndef CONFIG_H
#define CONFIG_H

// %1 = random number 1..9999
#define FS_IRC_URI "${FSIRC_DEF_URI}"
#define FS_REALNAME ${FSIRC_REALNAME}
#define FS_NICK "${FSIRC_NICK}"
#define FS_IDENT "${FSIRC_IDENT}"
#define FS_VERSION "${FSIRC_VERSION}"
#define FS_COM_CHAR "${FSIRC_COM_CHAR}"
#define FS_VERSION_REPLY ${FSIRC_VERSION_REPLY}
#define FS_QUIT_MSG ${FSIRC_QUIT_MSG}

#endif
