TEMPLATE = app
TARGET =
DEPENDPATH += . src
INCLUDEPATH += . src
QT += network svg xml
VERSION = "3.3.0"
MOC_DIR = .moc
OBJECTS_DIR = .obj
RCC_DIR = .res
UI_DIR = .ui

system(git describe) {
	VERSION=$$system(git describe)
}

include(custom.pri)

DEFINES += 'FS_VERSION=\\"$$VERSION\\"'

# Input
HEADERS += src/fscmdedit.h \
           src/fsettings.h \
           src/fsirc.h \
           src/fsircview.h \
           src/fstrayicon.h \
           src/irc.h \
           src/ircdefs.h \
           src/ircserver.h
FORMS += src/fsircview.ui src/fsmain.ui
SOURCES += src/fscmdedit.cpp \
           src/fsettings.cpp \
           src/fsirc.cpp \
           src/fsircview.cpp \
           src/fstrayicon.cpp \
           src/irc.cpp \
           src/ircserver.cpp \
           src/main.cpp
RESOURCES += src/fsirc.qrc
