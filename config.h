#define FS_IRC_URI "irc://irc.freenode.net/#fsirc"
#define FS_REALNAME "Fsirc"
#define FS_NICK "fsirc%1"
#define FS_IDENT "fsirc"
#define FS_COM_CHAR "/"
#define FS_VERSION_REPLY tr("VERSION fsirc %1, using Qt v %2").arg(FS_VERSION,QT_VERSION_STR)
#define FS_QUIT_MSG tr("farewell")